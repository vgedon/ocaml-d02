let sequence n =
	let decompress (x,y)=
		[string_of_int (x);y]
	in
	let rec tuplfy l = match l with
		| [] -> []
		| head::tail -> (1 , head):: tuplfy tail
	in
	let rec listify l = match l with
		| [] -> []
		| head::tail -> decompress head @ listify tail
	in
	let is_equal (_, c) (_,x) = c = x in
	let incr (i, e) = (i + 1, e) in
	let rec compress l = match l with
		| [] -> []
		| head::sec::tail when is_equal head sec -> compress ( incr head :: tail)
		| head::tail -> head :: compress tail
	
	in
	 let rec stringify l = match l with
		| [] -> ""
		| head::tail -> head ^ (stringify tail)
	in 
	let rec loop n l =
		if n > 1 then
			loop (n - 1) (listify (compress (tuplfy l) ) )
		else stringify l
	in loop n ["1"]


let () = print_endline (sequence 1)
let () = print_endline (sequence 2)
let () = print_endline (sequence 3)
let () = print_endline (sequence 4)
let () = print_endline (sequence 5)
let () = print_endline (sequence 6)
let () = print_endline (sequence 7)
let () = print_endline (sequence 8)
let () = print_endline (sequence 9)
let () = print_endline (sequence 10)
let () = print_endline (sequence 11)
let () = print_endline (sequence 12)
let () = print_endline (sequence 13)
