let rec encode lst =
	let is_equal (_, c) (_,x) = c = x in
	let incr (i, e) = (i + 1, e) in
	let rec tuplfy l = match l with
		| [] -> []
		| head::tail -> (1 , head):: encode tail
	in 
	let rec compress l = match l with
		| [] -> []
		| head::sec::tail when is_equal head sec -> compress ( incr sec :: tail)
		| head::tail -> head :: compress tail
	
	in compress (tuplfy lst)

let print_intl l = 
	List.iter print_int l

let print_charl l = 
	List.iter print_char l

let print_stringl l = 
	List.iter print_string l

let rec print_tuplecl l = 
	match l with
	| head::tail -> let print (i, c) = if i >= 1 then print_int i; print_char c
					in print head ; print_tuplecl tail
	| [] -> print_newline ()

let rec print_tupleil l = 
	match l with
	| head::tail -> let print (i, c) = if i >= 1 then print_int i; print_int c
					in print head ; print_tupleil tail
	| [] -> print_newline ()

let rec print_tuplesl l = 
	match l with
	| head::tail -> let print (i, c) = if i >= 1 then print_int i; print_string c
					in print head ; print_tuplesl tail
	| [] -> print_newline ()

let () = 
	let x = [] in print_charl x; print_string " => "; print_tuplecl (encode x);
	let x = ['a';'b';'c'] in print_charl x; print_string " => "; print_tuplecl (encode x);
	let x = ['a';'a';'a'] in print_charl x; print_string " => "; print_tuplecl (encode x);
	let x = ['b';'a';'a'] in print_charl x; print_string " => "; print_tuplecl (encode x);
	let x = ['a';'b';'b';'c';'c';'c';'d';'d';'d';'d'] in print_charl x; print_string " => "; print_tuplecl (encode x);
	let x = [0;1;1;2;2;2;3;3;3;3] in print_intl x; print_string " => "; print_tupleil (encode x);
	let x = ["hello ";"hello ";"world"] in print_stringl x; print_string " => "; print_tuplesl (encode x)