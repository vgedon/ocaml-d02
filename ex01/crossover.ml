let rec crossover l1 l2 =
	let rec is_in_list a l = match l with
	| [] -> false
	| head::tail -> head = a || is_in_list a tail
	in let rec cross l1 l2 = match l1 with
	| [] -> []
	| head::tail when (is_in_list head l2) -> head :: cross tail l2
	| head::tail -> cross tail l2
	in
	cross l2 (cross l1 l2)



let () = List.iter (fun x -> Printf.printf "%d " x) (crossover [0;1;2;3;4;5] []); print_newline ()
let () = List.iter (fun x -> Printf.printf "%d " x) (crossover [] [0;1;2;3;4;5]); print_newline ()
let () = List.iter (fun x -> Printf.printf "%d " x) (crossover [0;1;2;3;4;5;1;2;3;4;5;1;2;3;4;5] [1;3;5]); print_newline ()
let () = List.iter (fun x -> Printf.printf "%d " x) (crossover [0;1;2;3;4;5] [1;3;5;7;2]); print_newline ()
let () = List.iter (fun x -> Printf.printf "%c " x) (crossover ['a';'t';'t';'c';'g';'b'] ['a';'t';'c';'g']); print_newline ()